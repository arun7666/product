from django.forms import ModelForm
from django import forms
from user_panel.models import (
	CategorySubcategory,Product
    )

class CategorySubcategorymasterForm(ModelForm):
    class Meta:
        model =CategorySubcategory
        fields = "__all__"
        exclude = ['id','created_by','category_id']
    name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))



class CategorySubcategoryUpdateForm(ModelForm):
    class Meta:
        model =CategorySubcategory
        fields = "__all__"
        exclude = ['id','created_by']
    name = forms.CharField(required=False,widget=forms.TextInput(attrs={'class':'form-control'}))


 