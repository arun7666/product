from django.shortcuts import render
from rest_framework import status
from rest_framework import generics
from rest_framework import mixins
from user_panel.serializers import ProductSerializer,CategorySerializer
from rest_framework import viewsets
from rest_framework.authentication import SessionAuthentication,BasicAuthentication 
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
from django_filters.rest_framework import DjangoFilterBackend
from user_panel.models import CategorySubcategory,Product
from django.views.generic import TemplateView,CreateView
from user_panel.forms import CategorySubcategorymasterForm,CategorySubcategoryUpdateForm
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.http import HttpResponse,JsonResponse
class CategoryView(
	generics.GenericAPIView,
	mixins.ListModelMixin,
	mixins.RetrieveModelMixin,
	mixins.CreateModelMixin,
	mixins.UpdateModelMixin,
	mixins.DestroyModelMixin):

	serializer_class = CategorySerializer
	queryset = CategorySubcategory.objects.all()
	lookup_field = 'id'
	authentication_classes = [SessionAuthentication,BasicAuthentication]
	#permission_classes =[IsAuthenticated]
	def get(self,request,id=None):
		if id:
			return self.retrieve(request,id)
		else:
			return self.list(request)

	@permission_classes([IsAuthenticated])
	def post(self,request,id=None):
		return self.create(request)

	@permission_classes([IsAuthenticated])
	def put(self,request,id=None):
		return self.update(request,id)
	@permission_classes([IsAuthenticated])
	def delete(self,request,id=None):
		return self.destroy(request,id)

@method_decorator(login_required, name='dispatch')
class Dashboard(TemplateView):
	template_name = "base.html"

@method_decorator(login_required, name='dispatch')
class CategorySubcategoryMaster(CreateView):
    model = CategorySubcategory
    form_class = CategorySubcategorymasterForm
    template_name = "category/create.html"
    success_url = "/create_category/"
    def post(self,request,*args,**kwargs):
        form = CategorySubcategorymasterForm(self.request.POST)
        if form.is_valid():
            form.instance.created_by = self.request.user
            cat_type = request.POST.get('optradio')
            if cat_type == 'category':
                 form.instance.save()
                 return redirect(self.success_url)
            else:
                title = request.POST.get('name')
                cat_id = request.POST.getlist('name')
                for i in cat_id:
                    CategorySubcategory.objects.create(created_by = self.request.user,title = title,category_id = i)  
                else:
                    print("Broken For loop")
                return redirect(self.success_url)
        else:
            return render(request, self.template_name, {'form': form})
    def get_context_data(self,*args,**kwargs):
        context = super().get_context_data(**kwargs)
        dictionary = []
        data = CategorySubcategory.objects.filter(category_id__isnull=True).values('id','name')
        for i in data:
            title = i['name']
            cat_id = i['id']
            dictionary.append({"category":title,"id":cat_id, "sub_category":CategorySubcategory.objects.filter(category_id=cat_id).values('id','name')})
        #print("***************************************",context)
        context['category'] = CategorySubcategory.objects.filter(category_id__isnull=True).values('id','name')
        #print("*******************",dictionary)
        context.update({'data': dictionary})
        return context

@login_required
def edit_category_sub_category(request):
	if request.metho=='POST':
		print("/*/*/*/",request.GET.get('cat_id'))
		data = CategorySubcategory.objects.filter(id=request.GET.get('cat_id')).update()
		return JsonResponse(list(data),safe=False)
	else:
		print("/*/*/*/",request.GET.get('cat_id'))
		data = CategorySubcategory.objects.filter(id=request.GET.get('cat_id')).values()
		return JsonResponse(list(data),safe=False)
	