from django.urls import path
from user_panel.views import (
	CategoryView,Dashboard,
	CategorySubcategoryMaster,edit_category_sub_category
	)

app_name = "user_panel"

urlpatterns = [
	path('api/category/',CategoryView.as_view(),name="category"),
	path('api/category/<id>/',CategoryView.as_view(),name="category"),
	path('dashboard/',Dashboard.as_view(),name="dashboard"),
	path('create_category/',CategorySubcategoryMaster.as_view(),name="create_category"),
	path('edit_cat_sub_cat/',edit_category_sub_category,name="edit_cat_sub_cat"),
]
