from rest_framework import serializers
from user_panel.models import CategorySubcategory,Product
from django.contrib.auth.models import User

# Serializers define the API representation.
# class UserSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = User
#         fields = ('username', 'password')


class ProductSerializer(serializers.ModelSerializer):
	class Meta:
		ordering = ['-id']
		model = Product
		fields = ("id","created_by","category","name","price")
		extra_kwargs = {"category":{"required":False}}



class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True,read_only=True)
    class Meta:
    	ordering = ['-id']
    	model = CategorySubcategory
    	fields = ('id','created_by','category_id','name','products')
    	extra_kwargs = {"products":{'required':False}}