from django.db import models
from django.contrib.auth.models import User

class CategorySubcategory(models.Model):
	created_by = models.ForeignKey(User,on_delete=models.CASCADE)
	name = models.CharField(max_length=200,blank=True)
	category_id = models.PositiveIntegerField(blank=True,null=True)
	def __str__(self):
		return self.name

class Product(models.Model):
	created_by = models.ForeignKey(User,on_delete=models.CASCADE)
	category = models.ManyToManyField(CategorySubcategory)
	name = models.CharField(max_length=200,blank=True)
	price = models.PositiveIntegerField(blank=True)

	def __str__(self):
		return self.name