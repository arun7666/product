from django.contrib import admin

from user_panel.models import CategorySubcategory,Product

admin.site.register(CategorySubcategory)
admin.site.register(Product)